// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Duration } from "./Duration"

export class DutyTime {
	readonly dutyDate: Date
	get time(): Duration {
		return Duration.difference(this.date, this.dutyDate)
	}
	constructor(public readonly date: Date, dutyDate?: Date) {
		dutyDate = dutyDate ? dutyDate :  new Date(this.date.valueOf())
		dutyDate.setHours(0, 0, 0, 0)
		this.dutyDate = dutyDate
	}
	setTime(value: Duration): DutyTime {
		return new DutyTime(value.add(this.dutyDate), this.dutyDate)
	}
	setDutyDate(value: Date): DutyTime {
		return DutyTime.fromDuty(value, this.time)
	}
	private format(value: number): string {
		let result = value.toString()
		if (result.length == 1)
			result = "0" + result
		return result
	}
	toString(): string {
		return this.dutyDate.getFullYear().toString() + "-" + this.format(this.dutyDate.getMonth() + 1) + "-" + this.format(this.dutyDate.getDate()) + " " + this.time.toString()
	}
	static parse(value: string): DutyTime | undefined {
		const splitted = value.split(/[T,\s]/, 2)
		const dutyDate = DutyTime.parseDate(splitted[0]) || new Date()
		const time = Duration.parse(splitted[1])
		return time ? DutyTime.fromDuty(dutyDate, time) : new DutyTime(dutyDate)
	}
	private static parseDate(value: string): Date | undefined {
		let result: Date | undefined
		const matched = value.match(/(\d{2,4})-(\d{2,4})-(\d{2,4})/)
		if (matched)
			result = new Date(Number.parseInt(matched[1]), Number.parseInt(matched[2]) - 1, Number.parseInt(matched[3]))
		return result
	}
	static fromDuty(dutyDate: Date, time: Duration): DutyTime {
		dutyDate.setHours(0, 0, 0, 0)
		return new DutyTime(time.add(dutyDate), dutyDate)
	}
}
