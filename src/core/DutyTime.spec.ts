// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { DutyTime } from "./DutyTime"
import { Duration } from "./Duration"

describe("DutyTime", () => {
	it("default duty date", () => {
		const now = new Date(2018, 6, 1, 13, 37)
		const value = new DutyTime(now)
		expect(value.date).toBe(now)
		expect(value.time.totalMinutes).toBe(Duration.fromMinutes(817).totalMinutes)
		expect(value.dutyDate.valueOf()).toBe(new Date(2018, 6, 1).valueOf())
	})
	it("set duty date", () => {
		const now = new Date(2018, 6, 1, 13, 37)
		let value = new DutyTime(now)
		value = value.setDutyDate(new Date(2018, 6, 1))
		expect(value.dutyDate).toEqual(new Date(2018, 6, 1))
		expect(value.time.totalMinutes).toBe(Duration.fromMinutes(817).totalMinutes)
		expect(value.dutyDate.valueOf()).toBe(new Date(2018, 6, 1).valueOf())
	})
	it("set time", () => {
		const now = new Date(2018, 6, 1, 13, 37)
		let value = new DutyTime(now)
		value = value.setTime(Duration.fromMinutes(42))
		expect(value.time.totalMinutes).toBe(Duration.fromMinutes(42).totalMinutes)
		expect(value.date.valueOf()).toBe(new Date(2018, 6, 1, 0, 42).valueOf())
		expect(value.dutyDate.valueOf()).toBe(new Date(2018, 6, 1).valueOf())
	})
	it("toString default", () => {
		const value = new DutyTime(new Date(2018, 5, 1, 13, 37))
		expect(value.time).toEqual(Duration.fromHours(13, 37))
		expect(value.toString()).toBe("2018-06-01 13:37")
	})
	it("toString explicit", () => {
		const value = new DutyTime(new Date(2018, 5, 1, 13, 37), new Date(2018, 5, 1))
		expect(value.time).toEqual(Duration.fromHours(13, 37))
		expect(value.toString()).toBe("2018-06-01 13:37")
	})
	it("toString day after", () => {
		const value = new DutyTime(new Date(2018, 5, 1, 13, 37), new Date(2018, 4, 31))
		expect(value.date).toEqual(new Date(2018, 5, 1, 13, 37))
		expect(value.dutyDate).toEqual(new Date(2018, 4, 31))
		expect(value.time.totalHours).toEqual(Duration.fromHours(37, 37).totalHours)
		expect(value.toString()).toBe("2018-05-31 37:37")
	})
	it("toString day before", () => {
		const value = new DutyTime(new Date(2018, 5, 1, 13, 37), new Date(2018, 5, 2))
		expect(value.toString()).toBe("2018-06-02 -13:37")
	})
	it("parse same day", () => {
		expect(DutyTime.parse("2018-06-02 13:37")).toEqual(DutyTime.fromDuty(new Date(2018, 5, 2), Duration.fromHours(13, 37)))
	})
	it("parse day before", () => {
		expect(DutyTime.parse("2018-06-02 -13:37")).toEqual(DutyTime.fromDuty(new Date(2018, 5, 2), Duration.fromDays(-1, 13, 37)))
	})
	it("parse day before", () => {
		expect(DutyTime.parse("2018-06-02 37:37")).toEqual(DutyTime.fromDuty(new Date(2018, 5, 2), Duration.fromHours(37, 37)))
	})
	it("parse date only", () => {
		expect(DutyTime.parse("2018-06-02")).toEqual(new DutyTime(new Date(2018, 5, 2)))
	})
})
