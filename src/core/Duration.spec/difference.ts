// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Duration } from "../Duration"

describe("Duration difference", () => {
	it("zero", () => {
		const now = Date.now()
		const value = Duration.difference(new Date(now), new Date(now))
		expect(value.totalMilliseconds).toBe(0)
	})
	it("1 second", () => {
		const now = Date.now()
		const value = Duration.difference(new Date(now + 1000), new Date(now))
		expect(value.totalMilliseconds).toBe(1000)
	})
	it("-1 second", () => {
		const now = Date.now()
		const value = Duration.difference(new Date(now), new Date(now + 1000))
		expect(value.totalMilliseconds).toBe(-1000)
	})
	it("2018-07-01 - 2018-07-01 13:30", () => {
		const left = new Date(2018, 6, 1)
		const right = new Date(2018, 6, 1, 13, 30)
		const value = Duration.difference(left, right)
		expect(value.totalHours).toBe(-13.5)
	})
	it("2018-07-01 13:30 - 2018-07-01", () => {
		const left = new Date(2018, 6, 1, 13, 30)
		const right = new Date(2018, 6, 1)
		const value = Duration.difference(left, right)
		expect(value.totalHours).toBe(13.5)
	})
	it("2018-07-01 - 2018-07-02 13:30", () => {
		const left = new Date(2018, 6, 1, 0, 0)
		const right = new Date(2018, 6, 2, 13, 30)
		const value = Duration.difference(left, right)
		expect(value.totalMinutes).toBe(-2250)
		expect(value.totalHours).toBe(-37.5)
	})
	it("2018-07-02 13:30 - 2018-07-01", () => {
		const left = new Date(2018, 6, 2, 13, 30)
		const right = new Date(2018, 6, 1)
		const value = Duration.difference(left, right)
		expect(value.totalHours).toBe(37.5)
	})
})
