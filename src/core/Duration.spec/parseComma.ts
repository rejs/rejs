// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Duration } from "../Duration"

describe("Duration parse with ,", () => {
	it("hours 1 digit", () => {
		const duration = Duration.parse("3,0")
		expect(duration).toBeTruthy()
		expect(duration!.hours).toBe(3)
		expect(duration!.minutes).toBe(0)
	})
	it("hours 2 digit", () => {
		const duration = Duration.parse("13,0")
		expect(duration).toBeTruthy()
		expect(duration!.hours).toBe(13)
		expect(duration!.minutes).toBe(0)
	})
	it("hours and minutes 3 digit", () => {
		const duration = Duration.parse("3,37")
		expect(duration).toBeTruthy()
		expect(duration!.hours).toBe(3)
		expect(duration!.minutes).toBe(22)
	})
	it("hours and minutes 4 digit", () => {
		const duration = Duration.parse("13,50")
		expect(duration).toBeTruthy()
		expect(duration!.hours).toBe(13)
		expect(duration!.minutes).toBe(30)
	})
	it("hours and minutes 5 digit", () => {
		const duration = Duration.parse("113,25")
		expect(duration).toBeTruthy()
		expect(duration!.toString()).toBe("113:15")
		expect(duration!.days).toBe(4)
		expect(duration!.hours).toBe(17)
		expect(duration!.minutes).toBe(15)
	})
})
