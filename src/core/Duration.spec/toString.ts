// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Duration } from "../Duration"

describe("Duration toString", () => {
	it("hours", () => {
		const value = Duration.fromHours(13).toString()
		expect(value).toBe("13:00")
	})
	it("hours and minutes", () => {
		const value = Duration.fromHours(13, 37).toString()
		expect(value).toBe("13:37")
	})
	it("minutes", () => {
		const value = Duration.fromMinutes(42).toString()
		expect(value).toBe("00:42")
	})
	it("hours, minutes and seconds", () => {
		const value = Duration.fromHours(13, 37, 42).toString()
		expect(value).toBe("13:37:42")
	})
	it("minutes and seconds", () => {
		const value = Duration.fromMinutes(37, 42).toString()
		expect(value).toBe("00:37:42")
	})
	it("hours, minutes, seconds and milliseconds", () => {
		const value = Duration.fromHours(13, 37, 42, 137).toString()
		expect(value).toBe("13:37:42.137")
	})
	it("negative day, hours and minutes", () => {
		const value = Duration.fromDays(-1, 13, 37).toString()
		expect(value).toBe("-13:37")
	})
	it("negative days, hours and minutes", () => {
		const value = Duration.fromDays(-2, 13, 37).toString()
		expect(value).toBe("-2 days 13:37")
	})
})
