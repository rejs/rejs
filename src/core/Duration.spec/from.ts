// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import { Duration } from "../Duration"

describe("Duration from", () => {
	it("seconds", () => {
		const duration = Duration.fromSeconds(10)
		expect(duration.seconds).toBe(10)
	})
	it("negative seconds", () => {
		const duration = Duration.fromSeconds(-10)
		expect(duration.totalSeconds).toBe(-10)
	})
	it("minutes", () => {
		const duration = Duration.fromMinutes(10)
		expect(duration.totalMinutes).toBe(10)
	})
	it("hours", () => {
		const duration = Duration.fromHours(10)
		expect(duration.totalHours).toBe(10)
	})
	it("negative hours", () => {
		const duration = Duration.fromHours(-10)
		expect(duration.totalHours).toBe(-10)
	})
	it("days", () => {
		const duration = Duration.fromDays(10, 12, 45)
		expect(duration.totalDays).toBe(10.53125)
	})
})
describe("Negative parts", () => {
	it("minutes", () => {
		const duration = Duration.fromDays(-1, 0, 42)
		expect(duration.days).toBe(-1)
		expect(duration.hours).toBe(0)
		expect(duration.minutes).toBe(42)
	})
	it("hours", () => {
		const duration = Duration.fromDays(-1, 23)
		expect(duration.days).toBe(-1)
		expect(duration.hours).toBe(23)
		expect(duration.minutes).toBe(0)
	})
})
