// The MIT License (MIT)
//
// Copyright (c) 2018 Arriva Sverige AB
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import * as math from "../math"

export class Duration {
	get totalSeconds(): number { return this.totalMilliseconds / 1000 }
	get totalMinutes(): number { return this.totalSeconds / 60 }
	get totalHours(): number { return this.totalMinutes / 60 }
	get totalDays(): number { return this.totalHours / 24 }

	get fullSeconds(): number { return Math.floor(this.totalSeconds) }
	get fullMinutes(): number { return Math.floor(this.totalMinutes) }
	get fullHours(): number { return Math.floor(this.totalHours) }

	get milliseconds(): number { return this.totalMilliseconds % 1000 }
	get seconds(): number { return math.modulo(Math.floor(this.totalSeconds), 60) }
	get minutes(): number { return math.modulo(Math.floor(this.totalMinutes), 60) }
	get hours(): number { return math.modulo(Math.floor(this.totalHours), 24) }
	get days(): number { return Math.floor(this.totalDays) }
	private constructor(public readonly totalMilliseconds: number) {
	}
	add(duration: Duration): Duration
	add(date: Date): Date
	add(other: Duration | Date): Duration | Date {
			return other instanceof Duration ? new Duration(this.totalMilliseconds + other.totalMilliseconds) : new Date(this.totalMilliseconds + other.valueOf())
	}
	substractFrom(duration: Duration): Duration
	substractFrom(date: Date): Date
	substractFrom(other: Duration | Date): Duration | Date {
			return other instanceof Duration ? new Duration(other.totalMilliseconds - this.totalMilliseconds) : new Date(other.valueOf() - this.totalMilliseconds)
	}
	private format(value: number): string {
		let result = value.toString()
		if (result.length == 1)
			result = "0" + result
		return result
	}
	toString() {
		let result = ""
		if (this.days < -1)
			result = this.days.toString() + " days " + this.format(this.hours) + ":" + this.format(this.minutes)
		else if (this.days < 0)
			result = "-" + this.format(this.hours) + ":" + this.format(this.minutes)
		else
			result = this.format(this.fullHours) + ":" + this.format(this.minutes)

		if (this.seconds > 0 || this.milliseconds > 0) {
			result += ":" + this.format(this.seconds)
			if (this.milliseconds > 0)
				result += "." + this.milliseconds.toString()
		}
		return result
	}
	static difference(left: Date, right: Date): Duration {
		return new Duration(left.valueOf() - right.valueOf())
	}
	static fromSeconds(seconds: number, milliseconds = 0) {
		return new Duration(seconds * 1000 + milliseconds)
	}
	static fromMinutes(minutes: number, seconds = 0, milliseconds = 0) {
		return new Duration((minutes * 60 + seconds) * 1000 + milliseconds)
	}
	static fromHours(hours: number, minutes = 0, seconds = 0, milliseconds = 0) {
		return new Duration(((hours * 60 +  minutes) * 60 + seconds) * 1000 + milliseconds)
	}
	static fromDays(days: number, hours = 0, minutes = 0, seconds = 0, milliseconds = 0) {
		return new Duration((((days * 24 + hours) * 60 +  minutes) * 60 + seconds) * 1000 + milliseconds)
	}
	static parse(value: string | undefined): Duration | undefined {
		let result: Duration | undefined
		if (value == "n") {
			const now = new Date()
			result = Duration.fromHours(now.getUTCHours(), now.getUTCMinutes())
		} else if (value && value != "") {
			const match = value.match(/^(-?)(\d*)(([:\.,])(\d{1,2}))?$/)
			if (match) {
					const days = match[1] == "-" ? -1 : 0
					if (!match[3] && !match[4] && !match[5])
						result = match[2].length < 3 ? Duration.fromDays(days, Number.parseInt(match[2])) : Duration.fromDays(days, Number.parseInt(match[2].substring(0, match[2].length - 2)), Number.parseInt(match[2].substring(match[2].length - 2)))
					else {
						switch (match[4]) {
							case ":":
								result = Duration.fromDays(days, Number.parseInt(match[2]), Number.parseInt(match[5]))
								break
							case ".":	case ",":
								result = Duration.fromDays(days, Number.parseFloat(match[2] + "." + match[5]))
								break
						}
					}
			}
		}
		return result
	}
}
